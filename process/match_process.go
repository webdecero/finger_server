package process

import (
	"errors"
	"finger_server/global"
	"fmt"
	"os/exec"
	"strings"
)

func MatchProcess(fingerPath string) (string, error) {
	output, err := exec.Command(global.FingerProgramPath+global.MatchCommand,
		global.DatabasePath, fingerPath).Output()

	if err != nil {
		fmt.Println("Error match process", err)
		return "not_found", err
	}
	result := string(output)
	match_id := extractId(result)
	if match_id == "not_found" {
		return match_id, errors.New("not match found")
	}
	return match_id, nil
}

func extractId(output string) string {
	aux1 := strings.Split(output, "ID: ")
	if len(aux1) > 1 {
		aux2 := strings.Split(aux1[1], " with score")
		if len(aux2) > 1 {
			return aux2[0]
		}
	}
	return "not_found"
}
