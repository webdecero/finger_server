package process

import (
	"finger_server/global"
	"fmt"
	"os/exec"
)

func EnrollProcess(fingerPath, subjectId, enrollType string) error {
	var enrollProcess = exec.Command(global.FingerProgramPath+global.EnrollCommand,
		global.DatabasePath, fingerPath, subjectId, enrollType)

	err := enrollProcess.Start()
	if err != nil {
		fmt.Println("Error enroll process")
		return err
	}
	err = enrollProcess.Wait()
	if err != nil {
		fmt.Println("Error enroll process")
		return err
	}
	exitCode := enrollProcess.ProcessState.ExitCode()
	fmt.Println("exited with code:", exitCode)
	return nil
}
