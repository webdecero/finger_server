package process

import (
	"finger_server/global"
	"fmt"
	"os/exec"
)

func ConverterProcess(fingerPath string) error {
	var converterProcess = exec.Command("python3", global.ConverterCommand,
		fingerPath)

	err := converterProcess.Start()
	if err != nil {
		fmt.Println("Converter process")
		return err
	}
	err = converterProcess.Wait()
	if err != nil {
		fmt.Println("Error Converter process")
		return err
	}
	exitCode := converterProcess.ProcessState.ExitCode()
	fmt.Println("exited with code:", exitCode)
	return nil
}
