package http_api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func ListenHttpServer(address, port string) {
	http.HandleFunc("/", Hello)
	http.HandleFunc("/echo", Echo)
	http.HandleFunc("/enroll", EnrollHandler)
	http.HandleFunc("/match", MatchHandler)
	http.HandleFunc("/delete", DeleteHandler)
	http.HandleFunc("/convert", ConverterHandler)
	println("listening at", address, ":", port)

	err := http.ListenAndServe(address+":"+port, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func Hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello")
}

func Echo(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]interface{})

	switch r.Method {
	case "POST":
		data := r.FormValue("data")
		response["data"] = data
	default:
		response["data"] = "only post method allowed"
		w.WriteHeader(http.StatusBadRequest)
	}
	json.NewEncoder(w).Encode(response)
}
