package http_api

import (
	"encoding/json"
	"errors"
	"finger_server/db"
	"fmt"
	"net/http"
	"sync"
)

func DeleteHandler(w http.ResponseWriter, r *http.Request) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		response := make(map[string]interface{})
		err := checkDeleteParams(w, r)
		if err != nil {
			response["success"] = false
			response["message"] = err.Error()
		} else {
			subjectId := r.FormValue("subjectId")
			fmt.Println("deleting", subjectId)
			err, deleted := db.DeleteSubject(subjectId)
			if err != nil {
				response["success"] = false
				response["message"] = err.Error()
			} else {
				response["success"] = true
				response["deleted"] = deleted
			}
		}

		json.NewEncoder(w).Encode(response)
		wg.Done()
	}()
	wg.Wait()
}

func checkDeleteParams(w http.ResponseWriter, r *http.Request) error {
	var errorMessage string
	subjectId := r.FormValue("subjectId")

	if subjectId == "" {
		errorMessage += " subjectId parameter is required (StudentIdJR + fingerprintId)"
	}
	if errorMessage != "" {
		return errors.New(errorMessage)
	}
	return nil
}
