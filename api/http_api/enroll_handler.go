package http_api

import (
	"encoding/json"
	"errors"
	"finger_server/process"
	"fmt"
	"net/http"
	"sync"
)

func EnrollHandler(w http.ResponseWriter, r *http.Request) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		response := make(map[string]interface{})
		err := checkEnrollParams(w, r)
		if err != nil {
			response["success"] = false
			response["message"] = err.Error()
		} else {
			fingerPath := r.FormValue("fingerPath")
			enrollType := r.FormValue("enrollType")
			subjectId := r.FormValue("subjectId")
			fmt.Println(fingerPath, subjectId)
			err = process.ConverterProcess(fingerPath)
			fmt.Println(err)

			err = process.EnrollProcess(fingerPath, subjectId, enrollType)

			if err != nil {
				response["success"] = false
				response["message"] = err.Error()
			} else {
				response["success"] = true
			}
		}

		json.NewEncoder(w).Encode(response)
		wg.Done()
	}()
	wg.Wait()
}

func checkEnrollParams(w http.ResponseWriter, r *http.Request) error {
	var errorMessage string
	fingerPath := r.FormValue("fingerPath")
	enrollType := r.FormValue("enrollType")
	subjectId := r.FormValue("subjectId")
	if fingerPath == "" {
		errorMessage += " fingePath parameter is required (/home/user_ftp/path_to_image)"
	}
	if enrollType == "" {
		errorMessage += " enrollType parameter is required (store, update)"
	} else if enrollType != "update" && enrollType != "store" {
		errorMessage += " enrollType must be either update or store"
	}
	if subjectId == "" {
		errorMessage += " subjectId parameter is required (StudentIdJR + fingerprintId)"
	}
	if errorMessage != "" {
		return errors.New(errorMessage)
	}
	return nil
}
