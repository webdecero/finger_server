package http_api

import (
	"encoding/json"
	"errors"
	"finger_server/process"
	"fmt"
	"net/http"
	"sync"
)

func MatchHandler(w http.ResponseWriter, r *http.Request) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		err := checkMatchParams(w, r)
		response := make(map[string]interface{})

		if err != nil {
			response["success"] = false
			response["match"] = "not_found"
			response["message"] = err.Error()
		} else {
			fingerPath := r.FormValue("fingerPath")
			err := process.ConverterProcess(fingerPath)
			fmt.Println(err)
			id, err := process.MatchProcess(fingerPath)
			if err != nil {
				response["success"] = false
				response["match"] = id
				response["message"] = err.Error()
			} else {
				response["success"] = true
				response["match"] = id
			}
		}

		json.NewEncoder(w).Encode(response)
		wg.Done()
	}()
	wg.Wait()
}

func checkMatchParams(w http.ResponseWriter, r *http.Request) error {
	fingerPath := r.FormValue("fingerPath")
	var errorMessage string
	if fingerPath == "" {
		errorMessage += "  fingePath parameter is required (/home/user_ftp/path_to_image)"
	}
	if errorMessage != "" {
		return errors.New(errorMessage)
	}
	return nil
}
