package http_api

import (
	"encoding/json"
	"errors"
	"finger_server/process"
	"fmt"
	"net/http"
	"sync"
)

func ConverterHandler(w http.ResponseWriter, r *http.Request) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		response := make(map[string]interface{})
		err := checkConverterParams(w, r)
		if err != nil {
			response["success"] = false
			response["message"] = err.Error()
		} else {
			fingerPath := r.FormValue("fingerPath")
			fmt.Println(fingerPath)
			err = process.ConverterProcess(fingerPath)

			if err != nil {
				response["success"] = false
				response["message"] = err.Error()
			} else {
				response["success"] = true
			}
		}

		json.NewEncoder(w).Encode(response)
		wg.Done()
	}()
	wg.Wait()
}

func checkConverterParams(w http.ResponseWriter, r *http.Request) error {
	var errorMessage string
	fingerPath := r.FormValue("fingerPath")
	if fingerPath == "" {
		errorMessage += " fingePath parameter is required (/home/user_ftp/path_to_image)"
	}

	if errorMessage != "" {
		return errors.New(errorMessage)
	}
	return nil
}
