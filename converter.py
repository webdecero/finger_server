from PIL import Image
import sys


if __name__ == "__main__":
    print(sys.argv)
    imagePath = sys.argv[1]
    im = Image.open(imagePath)
    im = im.resize((314, 345))
    im.save(imagePath, dpi=(500, 500))