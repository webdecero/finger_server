package main

import (
	"finger_server/api/http_api"
	"finger_server/conf"
	"finger_server/global"
	"os"
)

func main() {

	conf.ReadConfFile(os.Args[1])
	http_api.ListenHttpServer(global.ServerIp, global.ServerPort)
}
