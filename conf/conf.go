package conf

import (
	"encoding/json"
	"errors"
	"finger_server/global"
	"io/ioutil"
	"log"
)

func ReadConfFile(conf_file string) {
	bytes, err := ioutil.ReadFile(conf_file)
	if err != nil {
		log.Fatal("server_conf.json file not found" + conf_file)
	}
	data := make(map[string]interface{})
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		log.Fatal(err)
	}

	checkConfigParams(data)
	checkValidParams()
}

func checkConfigParams(data map[string]interface{}) {
	var errorMessage string
	if v, ok := data["fingerProgramPath"]; ok {
		global.FingerProgramPath = v.(string)
	} else {
		errorMessage += " fingerProgramPath param missing in server_conf.json file"
	}
	if v, ok := data["converterCommand"]; ok {
		global.ConverterCommand = v.(string)
	} else {
		errorMessage += " converterCommand param missing in server_conf.json file"
	}
	if v, ok := data["databasePath"]; ok {
		global.DatabasePath = v.(string)
	} else {
		errorMessage += " databasePath param missing  in server_conf.json file"
	}
	if v, ok := data["enrollCommand"]; ok {
		global.EnrollCommand = v.(string)
	} else {
		errorMessage += " enrollCommand param missing in server_conf.json file"
	}
	if v, ok := data["matchCommand"]; ok {
		global.MatchCommand = v.(string)
	} else {
		errorMessage += " matchCommand param missing in server_conf.json file"
	}
	if v, ok := data["serverIp"]; ok {
		global.ServerIp = v.(string)
	} else {
		errorMessage += " serverIp param missing in server_conf.json file"
	}
	if v, ok := data["serverPort"]; ok {
		global.ServerPort = v.(string)
	} else {
		errorMessage += " serverPort param missing in server_conf.json file"
	}
	if errorMessage != "" {
		log.Fatal(errors.New(errorMessage))
	}
}

func checkValidParams() {
	if global.FingerProgramPath == "" {
		log.Fatal(errors.New("fingerProgramPath parameter is required"))
	}
	if global.ConverterCommand == "" {
		log.Fatal(errors.New("converterCommand parameter is required"))
	}
	if global.FingerProgramPath == "" {
		log.Fatal(errors.New("databasePath parameter is required"))
	}
	if global.FingerProgramPath == "" {
		log.Fatal(errors.New("enrollCommand parameter is required"))
	}
	if global.FingerProgramPath == "" {
		log.Fatal(errors.New("matchCommand parameter is required"))
	}
	if global.ServerIp == "" {
		log.Fatal(errors.New("serverIp parameter is required"))
	}
	if global.ServerPort == "" {
		log.Fatal(errors.New("serverPort parameter is required"))
	}
}
