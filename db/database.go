package db

import (
	"database/sql"
	"finger_server/global"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func DeleteSubject(subjectId string) (error, bool) {
	db, err := sql.Open("sqlite3", global.DatabasePath)
	defer db.Close()
	if err != nil {
		return err, false
	}
	stmt, err := db.Prepare("delete from Subjects where SubjectId = ?")
	if err != nil {
		return err, false
	}
	res, err := stmt.Exec(subjectId)
	if err != nil {
		return err, false
	}
	deleted, err := res.RowsAffected()
	if err != nil {
		return err, false
	}
	fmt.Println(deleted)

	return nil, deleted > 0
}
