#!/bin/bash

docker container start finger_server
docker cp main finger_server:/home/finger_server
docker cp server_conf.json finger_server:/home/finger_server
docker cp Linux_x86_64/enroll_fingerprint finger_server:/home/fingerprintcpp/Bin/Linux_x86_64
docker cp Linux_x86_64/match_fingerprint finger_server:/home/fingerprintcpp/Bin/Linux_x86_64
docker container exec -d finger_server /home/fingerprintcpp/Bin/Activation/Linux_x86_64/run_pgd.sh start
docker container exec -d finger_server /home/finger_server/main /home/finger_server/server_conf.json